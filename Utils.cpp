#include "Utils.h"

// Generate a random number in range of min to max
double random_double(double min, double max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> distr(min, max);
    return distr(gen);
}

// Select a sublist of a list
double* random_choice(double** array)
{   
    int length = sizeof(array) / sizeof(int); //get size of the array

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, length-1); 

    int random_index = dis(gen); 
    return array[random_index];
}

// average fitness of a population
double avg_fitness_pop(double** population)
{
    int length = sizeof(population) / sizeof(int); //get size of the array
        double avg_pop_fitness = 0.0;
        for (i = 0; i< length; i++)
        {
            avg_pop_fitness += population[i][2];
        }
    return avg_pop_fitness / length;
}

// return the lowest value of fittes individual in the population
double best_pop_fitness(double** population)
{
    double min = population[0][2];
    for (int i = 1; i < population.size(); i++) {
        if (population[i][2] < min) {
            min = population[i][2];
        }
    return min;
}