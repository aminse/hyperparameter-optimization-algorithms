#pragma once
#include <random>

double random_double(double min, double max);

double* random_choice(double** array);

double avg_fitness_pop(double** population);

double best_pop_fitness(double** population);

