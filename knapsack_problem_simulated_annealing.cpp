#include <iostream>
#include <ctime>

#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>


class Item
{
public:
    Item(double value, double weight, bool is_present);
    double get_value() const;
    double get_weight() const;
    bool is_present() const;
    void set_present(bool new_value);
    std::string to_string() const;
private:
    double member_value, member_weight;
    bool member_is_present;
};
Item::Item(double value, double weight, bool is_present): member_value{value}, member_weight{weight}, member_is_present{is_present}
{
}

double Item::get_value() const
{
    return member_value;
}
double Item::get_weight() const
{
    return member_weight;
}

bool Item::is_present() const
{
    return member_is_present;
}

void Item::set_present(bool new_value) { member_is_present = new_value; }


std::string Item::to_string() const
{
    std::ostringstream stream;
    stream << "w = " << member_weight << " v = " << member_value << " p = " << member_is_present;
    return stream.str();
}

class Solution
{
public:
    Solution(std::vector<Item> items);
    std::string to_string() const;
    double value() const;
    Solution random_walk() const;
private:
    double get_sum_weight() const;
private: 
    std::vector<Item> member_items;
    static const unsigned maximum_weight;
};

const unsigned Solution::maximum_weight = 100;


Solution::Solution(std::vector<Item> items): member_items{items}
{
}

std::string Solution::to_string() const
{
    std::ostringstream stream;
    stream << "Items: ";
    for (Item const& i : member_items)
    {
        stream << i.to_string() << " ";
    }
    stream << std::endl << " Total weight & value = " << get_sum_weight() << ' ' << value() << std::endl;

    return stream.str();
}


double Solution::get_sum_weight() const
{
    double sum_weight{ 0 };
    for (Item const& i : member_items)
    {
        if(i.is_present())
            sum_weight += i.get_weight();
    }
    return sum_weight;
}
double Solution::value() const
{
    double sum_value{ 0 };
    for (Item const& i : member_items)
    {
        if(i.is_present())
            sum_value += i.get_value();
    }
    if (get_sum_weight() > maximum_weight)
    {
        return -10;
    }
    return sum_value;
}

Solution Solution::random_walk() const
{
    std::vector<Item> copy_items = this->member_items;
    unsigned index = rand() % copy_items.size();

    copy_items[index].set_present(1 - copy_items[index].is_present());

    Solution new_solution(copy_items);
    while (new_solution.value() < 0)
    {
        for (Item& i : copy_items)
        {
            if (i.is_present())
            {
                i.set_present(false);
                break;
            }
        }
        new_solution.member_items = copy_items;
    }

    return new_solution;
}

bool random_bool()
{
    if (rand() % 2)
    {
        return true;
    }
    return false;
}

void simulated_annealing()
{
    const double initial_temperature = 100;
    const unsigned times_decrease_temp = 100;//100

    const unsigned neighbours = 10;//10
    const double decrease_amount = 0.85;

    unsigned yes = 0, no = 0;

    double prob_temp = 0;

    srand(time(NULL));

    //generate random first solution
    std::vector<Item> items
    {
        {11, 10, random_bool()},
        {12, 30, random_bool()},
        {14, 3,  random_bool()},
        {19, 21, random_bool()},
        {15,  13, random_bool()},
        {8, 14, random_bool()},
        {14,  4, random_bool()}
    };
    Solution current_solution(items);//random

    std::ofstream csv("results.csv", std::ofstream::out);
    double current_temperature = initial_temperature;
    for (unsigned i = 0; i < times_decrease_temp; i++)
    {
        std::cout << std::endl << "-------------------------Temperature = " << current_temperature << std::endl;
        std::cout << "Solution = " << current_solution.to_string() << std::endl;
        for (unsigned j = 0; j < neighbours; j++)
        {
            Solution candidate = current_solution.random_walk();
            std::cout << "Candidate = " << candidate.to_string() << std::endl;
            if (candidate.value() > current_solution.value())// notice inverted sign
            {
                std::cout << "Found a better solution" << std::endl;
                current_solution = candidate;
            }
            else if(candidate.value()>0)
            {
                double frac =  - candidate.value() + current_solution.value();//notice inverted sign
                double probability = 1 / exp(frac/current_temperature);

                std::cout << "Probability = " << probability << std::endl;
                prob_temp = probability;

                double r = (double)(rand() % 1000) / 1000;

                if (r < probability)
                {
                    std::cout << "Taking the worse solution" << std::endl;
                    current_solution = candidate;
                    yes++;
                }
                else
                {
                    std::cout << "NOT taking the worse solution" << std::endl;
                    no++;
                }
            }
        }
        std::cout << " Worse solution? Yes " << yes << " No " << no << std::endl;
        current_temperature = current_temperature * decrease_amount;
        csv << current_solution.value() << "\t" << yes << "\t" << no << "\t" << prob_temp << '\n';
    }
    std::cout << "Finally, solution = " << current_solution.to_string() << std::endl;
    csv.close();
}

int main()
{
    simulated_annealing();
    return 0;
}; 
