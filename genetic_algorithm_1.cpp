// The program is designed to find the best solution to the following equation (6x^3 + 9y^2 + 90z) - 25 = 0

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

#define POPULATION_SIZE 1000
#define NUMBER_OF_GENERATIONS 1000

class GeneticAlgorithm
{
    private:
        int population_size;
        int number_of_generations;
    
    public:
        GeneticAlgorithm(int population_size, int number_of_generations);
        double foo(double x, double y, double z);
        double fitness(double x, double y, double z);
        void genetic_algorithm();
};

GeneticAlgorithm::GeneticAlgorithm(int population_size, int number_of_generations)
{
    this->population_size = population_size;
    this->number_of_generations = number_of_generations;
}

double GeneticAlgorithm::foo(double x, double y, double z)
{
    return ((6 * (x * x * x)) + (9 * (y * y)) + (90 * z)) - 25;
}

double GeneticAlgorithm::fitness(double x, double y, double z)
{
    double ans = this->foo(x, y, z);
    
    if (ans == 0)
    {
        return 99999;
    }
    else
    {
        return (1 / abs(ans));
    }
}

void GeneticAlgorithm::genetic_algorithm()
{
    double** solutions = new double*[this->population_size];
    
    //generate the first population
    for (int s = 0; s < this->population_size; s++)
    {
        solutions[s] = new double[3];
        solutions[s][0] = (rand() % 10000);
        solutions[s][1] = (rand() % 10000);
        solutions[s][2] = (rand() % 10000);
    }
    
    for (int i = 0; i < this->number_of_generations; i++)
    {
        double** ranked_solutions = new double*[this->population_size];
        
        //sort solutions and retain top 100
        for (int s = 0; s < this->population_size; s++)
        {
            ranked_solutions[s] = new double[4];
            ranked_solutions[s][0] = this->fitness(solutions[s][0], solutions[s][1], solutions[s][2]);
            ranked_solutions[s][1] = solutions[s][0];
            ranked_solutions[s][2] = solutions[s][1];
            ranked_solutions[s][3] = solutions[s][2];
        }
        
        for (int j = 0; j < this->population_size; j++)
        {
            for (int k = 0; k < this->population_size - 1; k++)
            {
                if (ranked_solutions[k][0] < ranked_solutions[k + 1][0])
                {
                    double temp = ranked_solutions[k][0];
                    ranked_solutions[k][0] = ranked_solutions[k + 1][0];
                    ranked_solutions[k + 1][0] = temp;
                    
                    temp = ranked_solutions[k][1];
                    ranked_solutions[k][1] = ranked_solutions[k + 1][1];
                    ranked_solutions[k + 1][1] = temp;
                    
                    temp = ranked_solutions[k][2];
                    ranked_solutions[k][2] = ranked_solutions[k + 1][2];
                    ranked_solutions[k + 1][2] = temp;
                    
                    temp = ranked_solutions[k][3];
                    ranked_solutions[k][3] = ranked_solutions[k + 1][3];
                    ranked_solutions[k + 1][3] = temp;
                }
            }
        }
        cout << "=== Gen " << i << " best solutions ===" << endl;
        cout << ranked_solutions[0][0] << " " << ranked_solutions[0][1] << " " << ranked_solutions[0][2] << " " << ranked_solutions[0][3] << endl;


        
        int selection_number = int(this->population_size / 10);
        double** best_solutions = new double*[selection_number];
        
        for (int j = 0; j < selection_number; j++)
        {
            best_solutions[j] = new double[4];
            best_solutions[j][0] = ranked_solutions[j][0];
            best_solutions[j][1] = ranked_solutions[j][1];
            best_solutions[j][2] = ranked_solutions[j][2];
            best_solutions[j][3] = ranked_solutions[j][3];
        }
        
        double* elements = new double[selection_number * 3];
        int counter = 0;
        
        for (int j = 0; j < selection_number; j++)
        {
            elements[counter] = best_solutions[j][1];
            counter++;
            elements[counter] = best_solutions[j][2];
            counter++;
            elements[counter] = best_solutions[j][3];
            counter++;
        }
        
        for (int j = 0; j < this->population_size; j++)
        {
            delete[] solutions[j];
        }
        delete[] solutions;
        
        solutions = new double*[this->population_size];
        
        //mutate the population
        for (int j = 0; j < this->population_size; j++)
        {
            solutions[j] = new double[3];
            solutions[j][0] = elements[(rand() % (selection_number * 3))] * (0.99 + (static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * (1.01 - 0.99));
            solutions[j][1] = elements[(rand() % (selection_number * 3))] * (0.99 + (static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * (1.01 - 0.99));
            solutions[j][2] = elements[(rand() % (selection_number * 3))] * (0.99 + (static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * (1.01 - 0.99));
        }
        
        for (int j = 0; j < this->population_size; j++)
        {
            delete[] ranked_solutions[j];
        }
        delete[] ranked_solutions;
        
        for (int j = 0; j < selection_number; j++)
        {
            delete[] best_solutions[j];
        }
        delete[] best_solutions;
    }
    
    double best_solution = 0;
    double** final_ranked_solutions = new double*[this->population_size];
    
    for (int j = 0; j < this->population_size; j++)
    {
        final_ranked_solutions[j] = new double[4];
        final_ranked_solutions[j][0] = this->fitness(solutions[j][0], solutions[j][1], solutions[j][2]);
        final_ranked_solutions[j][1] = solutions[j][0];
        final_ranked_solutions[j][2] = solutions[j][1];
        final_ranked_solutions[j][3] = solutions[j][2];
    }
    
    for (int j = 0; j < this->population_size; j++)
    {
        for (int k = 0; k < this->population_size - 1; k++)
        {
            if (final_ranked_solutions[k][0] > final_ranked_solutions[k + 1][0])
            {
                double temp = final_ranked_solutions[k][0];
                final_ranked_solutions[k][0] = final_ranked_solutions[k + 1][0];
                final_ranked_solutions[k + 1][0] = temp;
                
                temp = final_ranked_solutions[k][1];
                final_ranked_solutions[k][1] = final_ranked_solutions[k + 1][1];
                final_ranked_solutions[k + 1][1] = temp;
                
                temp = final_ranked_solutions[k][2];
                final_ranked_solutions[k][2] = final_ranked_solutions[k + 1][2];
                final_ranked_solutions[k + 1][2] = temp;
                
                temp = final_ranked_solutions[k][3];
                final_ranked_solutions[k][3] = final_ranked_solutions[k + 1][3];
                final_ranked_solutions[k + 1][3] = temp;
            }
        }
    }
    
    
    for (int j = 0; j < this->population_size; j++)
    {
        delete[] solutions[j];
        delete[] final_ranked_solutions[j];
    }
    delete[] solutions;
    delete[] final_ranked_solutions;
}

int main()
{
    srand(time(NULL));
    
    GeneticAlgorithm ga = GeneticAlgorithm(POPULATION_SIZE, NUMBER_OF_GENERATIONS);
    ga.genetic_algorithm();
    
    return 0;
}
