
import math

# define the himmelbleau equation
def himmelbleau(x,y):
    return (x**2 + y - 11)**2 + (x + y**2 - 7)**2

# define the finite difference method to estimate the gradient
def finite_difference(x, y, eps=1e-5):
    der_x = (himmelbleau(x + eps, y) - himmelbleau(x, y)) / eps
    der_y = (himmelbleau(x, y + eps) - himmelbleau(x, y)) / eps
    return der_x, der_y

# define the SGD optimizer
def sgd_optimizer(x, y, learning_rate=0.01, iterations=1000):
  # loop through the number of iterations
  for i in range(iterations):
    # calculate the gradient using finite differences
    der_x, der_y = finite_difference(x, y)
    # update the parameters
    x = x - learning_rate * der_x
    y = y - learning_rate * der_y
  # return the optimized parameters
  return x, y

# set the initial parameters
x = 0
y = 0

# run the optimizer
x_opt, y_opt = sgd_optimizer(x, y, learning_rate=0.01, iterations=1000)

# print the optimized parameters
print("Optimized parameters: x={}, y={}".format(x_opt, y_opt))

# print the optimized value of himmelbleau equation
print("Optimized value of himmelbleau equation: {}".format(himmelbleau(x_opt, y_opt)))
