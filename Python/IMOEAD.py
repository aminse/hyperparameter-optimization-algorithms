import random
import typing

class IMOEAD:
    def __init__(self, population_size: int, max_iterations: int):
        self.population_size = population_size
        self.max_iterations = max_iterations

    def optimize(self, lower_bounds: typing.List[float], upper_bounds: typing.List[float]) -> typing.List[typing.Tuple[float, float]]:
        # Initialize population
        population = [self.generate_random_solution(lower_bounds, upper_bounds) for _ in range(self.population_size)]

        # Evaluate objectives for each solution in population
        population = [(solution, optimize_two_objectives(*solution)) for solution in population]

        # Initialize weight vectors for decomposition
        weight_vectors = self.generate_weight_vectors()

        for iteration in range(self.max_iterations):
            # Update weight vectors based on decomposition approach
            weight_vectors = self.update_weight_vectors(weight_vectors, iteration)

            # Calculate scalarized objectives for each solution
            population = self.scalarize(population, weight_vectors)

            # Perform selection, crossover, and mutation to generate offspring population
            offspring = self.create_offspring_population(population)

            # Evaluate objectives for each solution in offspring population
            offspring = [(solution, optimize_two_objectives(*solution)) for solution in offspring]

            # Merge population and offspring and perform environmental selection
            population = self.environmental_selection(population + offspring)

        # Return Pareto front of solutions
        return self.pareto_front(population)

    def generate_random_solution(self, lower_bounds: typing.List[float], upper_bounds: typing.List[float]) -> typing.Tuple[float, float]:
        # Generate random solution within bounds
        x = random.uniform(lower_bounds[0], upper_bounds[0])
        y = random.uniform(lower_bounds[1], upper_bounds[1])

        return x, y

    def generate_weight_vectors(self) -> typing.List[typing.Tuple[float, float]]:
        # Generate initial weight vectors using random sampling approach
        weight_vectors = []
        for i in range(self.population_size):
            x = random.uniform(0, 1)
            y = 1 - x
            weight_vectors.append((x, y))
        return weight_vectors

    def update_weight_vectors(self, weight_vectors: typing.List[typing.Tuple[float, float]], iteration: int) -> typing.List[typing.Tuple[float, float]]:
        # Update weight vectors using adaptive decomposition approach
        for i, weight_vector in enumerate(weight_vectors):
            x, y = weight_vector
            x_new = x * (1 - math.exp(-iteration / self.max_iterations))
            y_new = y * (1 - math.exp(-iteration / self.max_iterations))
            weight_vectors[i] = (x_new, y_new)
        return weight_vectors

    def scalarize(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]], weight_vectors: typing.List[typing.Tuple[float, float]]) -> typing.List[typing.Tuple[typing.Tuple[float, float], float]]:
        # Calculate scalarized objectives for each solution
        scalarized_population = []
        for solution, objectives in population:
            x, y = objectives
            wx, wy = weight_vectors[i]
            scalarized_objective = wx * x + wy * y
            scalarized_population.append((solution, scalarized_objective))
        return scalarized_population

    def create_offspring_population(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], float]]) -> typing.List[typing.Tuple[float, float]]:
        # Perform selection, crossover, and mutation to generate offspring population
        offspring = []
        for _ in range(self.population_size):
            # Select solutions for crossover
            parents = self.selection(population)
            # Perform crossover to generate offspring
            child = self.crossover(parents)

            # Perform mutation on offspring
            child = self.mutation(child)

            offspring.append(child)
        return offspring

    def selection(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], float]]) -> typing.List[typing.Tuple[float, float]]:
        # Perform selection using binary tournament selection
        parents = []
        for _ in range(2):
            # Select two solutions at random
            candidate1 = random.choice(population)
            candidate2 = random.choice(population)

            # Select the solution with the better scalarized objective
            if candidate1[1] < candidate2[1]:
                parents.append(candidate1[0])
            else:
                parents.append(candidate2[0])
        return parents

    def crossover(self, parents: typing.List[typing.Tuple[float, float]]) -> typing.Tuple[float, float]:
        # Perform crossover using arithmetic crossover
        x1, y1 = parents[0]
        x2, y2 = parents[1]
        alpha = random.uniform(0, 1)
        x_new = alpha * x1 + (1 - alpha) * x2
        y_new = alpha * y1 + (1 - alpha) * y
    def mutation(self, solution: typing.Tuple[float, float]) -> typing.Tuple[float, float]:
        # Perform mutation using Gaussian mutation
        x, y = solution
        x_new = x + random.gauss(0, 1)
        y_new = y + random.gauss(0, 1)
        return x_new, y_new

    def environmental_selection(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]]) -> typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]]:
        # Perform environmental selection using non-dominated sorting
        fronts = self.non_dominated_sort(population)

        # Truncate fronts to fit within population size
        new_pop = []
        for front in fronts:
            if len(new_pop) + len(front) > self.population_size:
                max_individuals = self.population_size - len(new_pop)
                if max_individuals > 0:
                    new_pop += front[:max_individuals]
            else:
                new_pop += front

        return new_pop

def non_dominated_sort(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]]) -> typing.List[typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]]]:
        # Perform non-dominated sorting to partition population into fronts
        fronts = []
        S = [set() for _ in population]
        n = [0 for _ in population]
        front = []
        rank = [0 for _ in population]

        for i, p in enumerate(population):
            S[i] = set()
            n[i] = 0
            for j, q in enumerate(population):
                if self.dominates(q[1], p[1]):
                    S[i].add(j)
                elif self.dominates(p[1], q[1]):
                    n[i] += 1
            if n[i] == 0:
                rank[i] = 0
                front.append(i)

        # Add solutions in first front to the list of fronts
        fronts.append([population[i] for i in front])

        # Repeat process for subsequent fronts
        i = 0
        while front:
            Q = []
            for p in front:
                for q in S[p]:
                    n[q] -= 1
                    if n[q] == 0:
                        rank[q] = i + 1
                        Q.append(q)
            i += 1
            front = Q
            fronts.append([population[i] for i in front])

        return fronts
    def pareto_front(self, population: typing.List[typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]]) -> typing.List[typing.Tuple[float, float]]:
        # Extract solutions in the Pareto front
        pareto_front = [solution[0] for solution in population if self.is_pareto_optimal(solution)]
        return pareto_front

    def is_pareto_optimal(self, solution: typing.Tuple[typing.Tuple[float, float], typing.Tuple[float, float]]) -> bool:
        # Check if solution is Pareto optimal
        x1, y1 = solution[1]
        is_pareto_optimal = True
        for _, (x2, y2) in population:
            if x1 > x2 and y1 > y2:
                is_pareto_optimal = False
                break
        return is_pareto_optimal


#Driver code
#Set parameters for IMOEAD
max_iterations = 100
population_size = 50

# Create I-MOEA/D object
imoea = IMOEAD(max_iterations, population_size)

# Optimize function using I-MOEA/D
pareto_front = imoea.optimize(optimize_two_objectives)

# Print solutions in the Pareto front
for solution in pareto_front:
    print(solution)

