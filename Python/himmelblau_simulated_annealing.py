import random
import math

class Solution:
    def __init__(self, x = None, y = None):
        if x == None and y == None:
            self.x = random.randint(-5, 5)
            self.y = random.randint(-5, 5)
        else:
            self.x = x
            self.y = y

    def __str__(self):
        return "x = " + str(self.x) + " y = " + str(self.y) + " z = " + str(self.value())
    
    #Himmelblau's function
    def value(self):
        return pow((self.x * self.x) + self.y - 11, 2) + pow(self.x + (self.y * self.y) - 7, 2)

    def random_walk(self, max_step):
        step = random.randint(-2 * max_step * 1000, 2 * max_step * 1000) / 1000
        newX = self.x + step

        step = random.randint(-2 * max_step * 1000, 2 * max_step * 1000) / 1000
        newY = self.y + step

        return Solution(x = newX, y = newY)

def simulated_annealing():
    initial_temperature = 100
    decrease_steps = 100

    step = 1
    neighbours = 10
    decrease_amount = 0.85

    yes = 0
    no = 0

    prob_temp = 0

    current_solution = Solution(x = None, y = None) 

    file = open("results.csv", "w")
    current_temperature = initial_temperature
    for i in range(decrease_steps):
        print("Temperature = " + str(current_temperature))
        print("Solution = " + str(current_solution))
        for j in range(neighbours):
            candidate = current_solution.random_walk(step)
            print("Candidate = " + str(candidate))
            if candidate.value() < current_solution.value():
                print("Found a better solution")
                current_solution = candidate
            else:
                frac = candidate.value() - current_solution.value()
                try:
                    probability = 1 / math.exp(frac/current_temperature)
                except OverflowError:
                    probability = 1 / float('inf')

                print("Probability = " + str(probability))
                prob_temp = probability

                r = (random.randint(0, 1000) / 1000)

                if r < probability:
                    print("Taking the worse solution")
                    current_solution = candidate
                    yes = yes + 1
                else:
                    print("NOT taking the worse solution")
                    no = no + 1
        print("Worse solution? Yes " + str(yes) + " No " + str(no))
        current_temperature = current_temperature * decrease_amount
        file.write(str(current_solution.value()) + "\t" + str(yes) + "\t" + str(no) + "\t" + str(prob_temp) + '\n')
    print("Final solution = " + current_solution)
    file.close()

simulated_annealing()
