import random
import math

# define a class for the knapsack problem
class Knapsack:

    # initialize the problem
    def __init__(self, n, weight, value):
        self.n = n
        self.weight = weight
        self.value = value

    # define a function to randomly generate a solution
    def random_solution(self):
        solution = []
        for i in range(self.n):
            solution.append(random.randint(0, 1))
        return solution

    # define a function to calculate the value of a solution
    def value_solution(self, solution):
        value = 0
        for i in range(self.n):
            value += solution[i] * self.value[i]
        return value

    # define a function to calculate the weight of a solution
    def weight_solution(self, solution):
        weight = 0
        for i in range(self.n):
            weight += solution[i] * self.weight[i]
        return weight

    # define a function to calculate the fitness of a solution
    def fitness_solution(self, solution):
        # if the weight of the solution exceeds the capacity, the fitness is 0
        if self.weight_solution(solution) > capacity:
            return 0
        # otherwise, the fitness is the value of the solution
        else:
            return self.value_solution(solution)

    # define a function to mutate a solution
    def mutate_solution(self, solution):
        # pick a random item to flip
        i = random.randint(0, self.n-1)
        # flip the item
        if solution[i] == 1:
            solution[i] = 0
        else:
            solution[i] = 1
        return solution

    # define a function to calculate the probability of accepting a solution
    def probability(self, current_fitness, new_fitness, temperature):
        # if the new solution is better, always accept it
        if new_fitness > current_fitness:
            return 1.0
        # if the new solution is worse, calculate the probability of accepting it
        else:
            return math.exp((new_fitness - current_fitness) / temperature)

    # define a function to solve the problem using simulated annealing
    def solve(self, max_iterations, max_temperature, min_temperature):
        # initialize the solution and fitness
        current_solution = self.random_solution()
        current_fitness = self.fitness_solution(current_solution)
        # initialize the best solution and best fitness
        best_solution = current_solution
        best_fitness = current_fitness
        # set the temperature
        temperature = max_temperature
        # for each iteration
        for i in range(max_iterations):
            # mutate the solution
            new_solution = self.mutate_solution(current_solution)
            # calculate the fitness of the new solution
            new_fitness = self.fitness_solution(new_solution)
            # calculate the probability of accepting the new solution
            p = self.probability(current_fitness, new_fitness, temperature)
            # if the new solution is better, always accept it
            if new_fitness > current_fitness:
                current_solution = new_solution
                current_fitness = new_fitness
            # otherwise, accept the new solution with probability p
            elif random.random() < p:
                current_solution = new_solution
                current_fitness = new_fitness
            # if the current solution is better than the best solution, update the best solution
            if current_fitness > best_fitness:
                best_solution = current_solution
                best_fitness = current_fitness
            # decrease the temperature
            temperature *= 0.999
            # if the temperature has reached the minimum temperature, stop
            if temperature < min_temperature:
                break
        # return the best solution
        return best_solution

# set the capacity, number of items, weights, and values
capacity = 10
n = 5
weights = [1,2,3,4,5]
values = [5,3,2,4,1]

# create a knapsack problem and solve it
knapsack = Knapsack(n, weights, values)
solution = knapsack.solve(1000, 100, 0.01)

# print the best solution
print(solution)
