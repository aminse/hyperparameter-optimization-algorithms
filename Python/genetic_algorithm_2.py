import random
class GeneticAlgorithm:
    def __init__(self, pop_size, Pc, Pm, G, x_lower, x_upper, y_lower, y_upper):
        self.pop_size = pop_size
        self.Pc = Pc
        self.Pm = Pm
        self.G = G
        self.x_lower = x_lower
        self.x_upper = x_upper
        self.y_lower = y_lower
        self.y_upper = y_upper
    
    def Himmelblau(self, x, y):
        return (x**2 + y - 11)**2 + (x + y**2 - 7)**2
    
    def init_population(self):
        
        population = []
        for i in range(self.pop_size):
            x = random.uniform(self.x_lower, self.x_upper)
            y = random.uniform(self.y_lower, self.y_upper)
            population.append([x, y, self.Himmelblau(x, y)])
        return population
    
    def init_individual(self):
        x = random.uniform(self.x_lower, self.x_upper)
        y = random.uniform(self.y_lower, self.y_upper)
        return [x, y, self.Himmelblau(x, y)]
    
    def selection(self, population):
        population.sort(key = lambda individual: individual[2])
        selected = population[:int(self.pop_size)]
        while len(selected) < self.pop_size:
            selected.append(self.init_individual())
        return selected


    def crossover(self, population):
        new_population = []
        for i in range(0, len(population)-1, 2):
            avg_pop_fitness = int(sum(individual[2] for individual in population) / len(population))
            best_pop_fitness = min(individual[2] for individual in population)
            
            if random.uniform(0, 1) < self.Pc:
                try:
                    mfcta = int(avg_pop_fitness/population[i][2])
                    num_of_mating = int((best_pop_fitness / population[i][2]) * 10) if population[i][2] < avg_pop_fitness else 0
                    for j in range(0, num_of_mating):
                        x1, y1, f1 = population[i]
                        x2, y2, f2 = random.choice(population)
                        child1_x = (x1 + x2)/2
                        child1_y = (y1 + y2)/2
                        child1_f = self.Himmelblau(child1_x, child1_y)
                        child2_x = (x1 - x2)/2
                        child2_y = (y1 - y2)/2
                        child2_f = self.Himmelblau(child2_x, child2_y)
                        new_population.append([child1_x, child1_y, child1_f])
                        new_population.append([child2_x, child2_y, child2_f])
                except ZeroDivisionError as e:
                    return (f'found answer :{population[i]}')
            else:
                new_population.append(population[i])
                new_population.append(random.choice(population))
        return new_population
    
    def mutation(self, population):
        for individual in population:
            if random.uniform(0, 1) < self.Pm:
                x, y, f = individual
                mutated_x = random.uniform(self.x_lower, self.x_upper)
                mutated_y = random.uniform(self.y_lower, self.y_upper)
                mutated_f = self.Himmelblau(mutated_x, mutated_y)
                individual[0] = mutated_x
                individual[1] = mutated_y
                individual[2] = mutated_f
        return population
    
    def main(self):
        population = self.init_population()
        best_solution = [1, 1, self.Himmelblau(1, 1)]
        for i in range(self.G):
            print("Generation {}:".format(i))
            print(len(population))
            population = self.crossover(population)
            if "found" in population:
                print(population)
                break
            else:
                population = self.mutation(population)
                population = self.selection(population)
                best_of_current_generation = sorted(population, key = lambda individual: individual[2])[0]
                if best_of_current_generation[2] < best_solution[2]:
                    best_solution = best_of_current_generation
                print("Best solution of Generation {}: x = {}, y = {}, f(x, y) = {}\n".format(i, population[0][0], population[0][1], self.Himmelblau(population[0][0], population[0][1])))
        print(f"Best solution of all {self.G} generations: {best_solution}")

if __name__ == "__main__":
    ga = GeneticAlgorithm(1000, 0.9, 0.05, 10000, -10, 10, -10, 10)
    ga.main()
