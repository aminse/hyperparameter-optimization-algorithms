import numpy as np
import random
import time
import multiprocessing as mp

def check_tour(tour, n):
    m = len(tour)
    if m != n:
        return False
    counts = [0 for i in range(n)]
    for city in tour:
        if city < 0 or city > n-1 or counts[city] != 0:
            return False
        counts[city] += 1
    return True

def check_tours(tours, n):
    all_valid = True
    for t in tours:
        if not check_tour(t, n):
            all_valid = False
            break
    return all_valid


def iroulette(weights): 
    num_weights = len(weights)
    imax = -1;
    vmax = 0
    for i in range(num_weights):
        val = weights[i]* random.random()
        if val > vmax:
            vmax = val
            imax = i
    return imax
    
def construct_tour(weights):
    n = weights[0].size
    cur_city = random.randrange(0,n)
    tour = [cur_city]
    free = np.zeros(n) == 0.0
    free[cur_city] = False
    while len(tour) < n:
        w = []
        indices = []
        for i in range(n):
            if free[i]:
                w.append( weights[cur_city][i] )
                indices.append(i)
        sel = iroulette(w)
        cur_city = indices[sel]
        tour.append(cur_city)
        free[cur_city] = False
    return tour

def construct_tours(weights, num_ants):
    tours = []
    for i in range(num_ants):
        tours.append( construct_tour(weights))
    return tours

def np_construct_tour(weights):
    n = weights[0].size
    cur_city = random.randrange(0,n)
    tour = np.full(n,0)
    tour[0]=cur_city
    free= np.zeros(n) == 0.0
    free[cur_city] = False

    i=1
    while i < n:
        w = weights[cur_city][free]
        indices = np.arange(n)[free]
        sel = np.argmax(w*random.random())
        cur_city = indices[sel]
        tour[i]=cur_city
        i+=1
        free[cur_city] = False
    return tour.tolist()
    
def construct_tours_np(weights, num_ants):
    tours = []
    for i in range(num_ants):
        tours.append( np_construct_tour(weights) )
    return tours

def my_func(weights,q):
    q.put(np_construct_tour(weights))
    
def construct_tours_mp(weights, num_ants):
    num_procs = num_ants
    out_queue = mp.Queue()
    
    procs = [mp.Process(target=my_func,args=(weights,out_queue)) for p in range(num_procs)]
    for p in procs:
        p.start()
    
    tours=[out_queue.get() for p in procs]
    return tours

#plot path
def plot_path(x,y,tour,save=False,fname=''):
    import matplotlib.pyplot as plt
    plt.plot(x,y, marker='o', markerfacecolor='red', linestyle='dashed', color='green', markersize=12)
    for i in range(len(x)):
        plt.text(x[i],y[i],str(i),size=20)
    plt.grid()
    plt.plot( [x[tour[i]] for i in range(len(x)) ], [y[tour[i]] for i in range(len(y)) ], 'b-', linewidth=2)
    plt.plot( [x[tour[0]]], [y[tour[0]]], 'bo', linewidth=2)
    if save and len(fname) > 0:
        plt.savefig(fname)
    else:
        plt.show()
    plt.close()

def main():
    n = 8
    weights = np.random.random((n,n)) # square matrix of random weights for edges
    num_ants = 80

    print( 'Constructing tours with construct_tours()' )
    t0 = time.time()
    tours = construct_tours(weights, num_ants)
    print( '\tdone in {0} s'.format(time.time()-t0) )

    print( 'Constructing tours with construct_tours_np()')
    t0 = time.time()
    tours = construct_tours_np(weights, num_ants)
    print( '\tdone in {0} s'.format(time.time()-t0) )

    print( 'Constructing tours with construct_tours_mp()')
    t0 = time.time()
    tours = construct_tours_mp(weights, num_ants)
    print( '\tdone in {0} s'.format(time.time()-t0) )


    # Uncomment the following if you wish to plot the constructed tour for a graph with 100 nodes.
    # This will only work if you have matplotlib installed.
    if n <= 100:
        x = [random.uniform(0,100) for i in range(n)]
        y = [random.uniform(0,100) for i in range(n)]

        plot_path(x,y,tours[0])
    
    
    assert check_tours(tours, n)
    print( 'All good!' )

if __name__ == '__main__':
    main()
