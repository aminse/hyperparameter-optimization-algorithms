import random
import math
import matplotlib.pyplot as plt
 
#Problem definition
def himmelblau(x):
    return (x[0]**2 + x[1] - 11)**2 + (x[0] + x[1]**2 - 7)**2
 
#Differential Evolution
def differential_evolution(obj_func, bounds, mut, crossp, popsize, its):
    dimensions = len(bounds)
    pop = []
    for i in range(0, (popsize)):
        d = []
        for j in range(0, dimensions):
            d.append(random.uniform(bounds[j][0], bounds[j][1]))
        pop.append(d)
   
    for i in range(0, its):
        for j in range(0, popsize):
            idxs = [idx for idx in range(0, popsize) if idx != j]
            a=pop[random.sample(idxs, 1)[0]]
            b=pop[random.sample(idxs, 1)[0]]
            c=pop[random.sample(idxs, 1)[0]]
            mutant = []
            for k in range(0, dimensions):
                if (random.random() < crossp):
                    d = bounds[k][0] + (bounds[k][1] - bounds[k][0]) * random.random()
                    mutant.append(d)
                else:
                    mutant.append(pop[j][k])
           
            cross_points = sorted(random.sample(range(0, dimensions), 2))
            for k in range(cross_points[0], cross_points[1]):
                mutant[k] = a[k] + mut * (b[k] - c[k])
         
            d = pop[j]
            if (obj_func(mutant) < obj_func(d)):
                pop[j] = mutant
         
#         print(min([[obj_func(x), x] for x in pop]))
     
    return min([[obj_func(x), x] for x in pop])
 
#Parameters
mut = 0.5
crossp = 0.7
popsize = 40
its = 1000
 
#Run algorithm
bounds = [(-6, 6), (-6, 6)]
 
best = differential_evolution(himmelblau, bounds, mut, crossp, popsize, its)
print('Best solution: ' + str(best[1]))
print('Best value: ' + str(best[0]))

### plotting
# fig = plt.figure()
# ax = fig.add_subplot(111)
 
# # Make the contour plot
# xlist = np.linspace(-6, 6, 100)
# ylist = np.linspace(-6, 6, 100)
# X, Y = np.meshgrid(xlist, ylist)
# Z = himmelblau([X, Y])

# cs = plt.contour(X, Y, Z)
# plt.clabel(cs, inline=1, fontsize=10)
 
# # Add some text over some of the data points.
# ax.text(best[1][0], best[1][1], 'x')

# plt.xlabel('x')
# plt.ylabel('y')
# plt.show()
