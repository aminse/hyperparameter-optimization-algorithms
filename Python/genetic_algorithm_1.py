# The program is designed to find the best solution to the following equation (6x^3 + 9y^2 + 90z) - 25 = 0
import random
import typing

class GeneticAlgorithm:
    def __init__(self, population_size: int, number_of_generations: int):
        self.population_size = population_size
        self.number_of_generations = number_of_generations
        
    def foo(self, x: float, y: float, z: float) -> float:
        return ((6 * (x ** 3)) + (9 * (y ** 2)) + (90 * z)) - 25
    
    def fitness(self, x: float, y: float, z: float) -> float :
        ans = self.foo(z, y, z)
        
        if ans == 0.0:
            return 99999.0
        else:
            return (abs(1 / ans))
        
    def genetic_algorithm(self):
        solutions = []
        
        #generate the first population
        for s in range(self.population_size):
            solutions.append(((random.uniform(0, 10000)),
                             random.uniform(0, 10000),
                             random.uniform(0, 10000)))
                             
        for i in range(self.number_of_generations):
            #sort solutions and retain top 100
            ranked_solutions = []
            for s in solutions:
                ranked_solutions.append((self.fitness(s[0], s[1], s[2]), s))
            ranked_solutions.sort(reverse=True)
            

            selection_number = int(self.population_size / 10)
            best_solutions = ranked_solutions[:selection_number]
            
            elements = []
            for item in best_solutions:
                elements.append(item[1][0])
                elements.append(item[1][1])
                elements.append(item[1][2])
                
            new_gen = []
            for _ in range(self.population_size):
                #mutate the population
                e1 = random.choice(elements) * random.uniform(0.99, 1.01)
                e2 = random.choice(elements) * random.uniform(0.99, 1.01)
                e3 = random.choice(elements) * random.uniform(0.99, 1.01)
                
                new_gen.append((e1, e2, e3))
            
            solutions = new_gen
            
        print(ranked_solutions[0])

if __name__ == "__main__":
    ga = GeneticAlgorithm(1000, 1000)
    ga.genetic_algorithm()
