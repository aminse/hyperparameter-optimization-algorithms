from __future__ import division
from __future__ import print_function

from math import log, exp, fabs, sqrt
from csv import DictReader
from datetime import datetime
from random import random
from hashlib import sha256


def log_loss(y, p):
    p = max(min(p, 1. - 10e-15), 10e-15)
    return -log(p) if y == 1 else -log(1. - p)


def to_hash(value):
    if not isinstance(value, bytes):
        value = str(value).encode('utf-8')
    hex_value = sha256(value).hexdigest()
    int_hash = int(hex_value, 16)
    return int_hash


class DataGen(object):
    def __init__(self, max_features, target, descriptive=(), categorical=(), numerical=None, transformation=None):

        self.mf = max_features
        self.y = target
        self.ids = descriptive
        self.cat = categorical
        self.num = numerical
        self.tra = transformation if transformation is not None else {}
        self.names = {}

        if self.num is not None:
            self.num_cols = sorted(self.num)

            self.names.update(dict(zip(self.num_cols, range(len(self.num_cols)))))
        else:
            self.num_cols = []


        assert len(self.cat) + len(self.num_cols) > 0, 'At least one categorical or numerical feature must ' \
                                                       'be provided.'

    def _fetch(self, path):
        
        for t, row in enumerate(DictReader(open(path))):
            ids = []
            for ID in self.ids:
                ids.append(row[ID])
                del row[ID]

            y = 0.
            if self.y in row:
                if row[self.y] == '1':
                    y = 1.
                del row[self.y]

            x = {}

            if t == 0:
                num_size = len(self.num_cols)
                size = num_size + len(self.tra)

                assert self.mf > size, 'Not enough dimensions to fit all features.'


            for i, key in enumerate(self.num_cols):
                x[i] = float(row[key])


            for i, key in enumerate(self.tra):
                x[num_size + i] = self.tra[key][1](row[self.tra[key][0]])
                self.names[key] = num_size + i

            for key in self.cat:

                value = row[key]

                index = to_hash(key + '_' + value) % (self.mf - size) + size
                x[index] = 1.

                if key + '_' + value not in self.names and len(self.names) < self.mf:
                    self.names[key + '_' + value] = index

            yield t, ids, x, y

    def train(self, path):
        for t, ids, x, y in self._fetch(path):

            yield t, x, y

    def test(self, path):
        for t, ids, x, y in self._fetch(path):
            yield t, x


class FTRLP(object):

    def __init__(self, alpha=1, beta=1, l1=1, l2=1, subsample=1, epochs=1, rate=0):

        # Learning rate's proportionality constant.
        self.alpha = alpha
        # Learning rate's parameter.
        self.beta = beta
        # L1 regularization constant.
        self.l1 = l1
        # L2 regularization constant.
        self.l2 = l2

        self.log_likelihood_ = 0
        self.loss = []


        self.z = None
        self.n = None

        self.coef_ = {}
        self.cname = None

  
        self.target_ratio = 0.


        self.rate = rate

        self.subsample = subsample

        self.epochs = epochs

        self.fit_flag = False

    def _build_p(self, data_gen, path):
        pass

    def _clear_params(self):
        self.log_likelihood_ = 0
        self.loss = []
        self.z = None
        self.n = None
        self.coef_ = {}
        self.cname = None

    def get_params(self, deep=True):
        ps = {'alpha': self.alpha,
              'beta': self.beta,
              'l1': self.l1,
              'l2': self.l2,
              'subsample': self.subsample,
              'epochs': self.epochs,
              'rate': self.rate}

        return ps

    def set_params(self, **params):
        for key, value in params.iteritems():
            setattr(self, key, value)

    def _update(self, y, p, x, w):
        for i in x.keys():
            # --- Compute Gradient of LogLoss
            g = (p - y) * x[i]

            s = (sqrt(self.n[i] + g * g) - sqrt(self.n[i])) / self.alpha

            self.z[i] += g - s * w[i]
            self.n[i] += g * g

    def _train(self, data_gen, path):

        if self.z is None and self.n is None:
            self.z = [0.] * data_gen.mf
            self.n = [0.] * data_gen.mf

        start_time = datetime.now()

        for t, x, y in data_gen.train(path):
            self.target_ratio = (1.0 * (t * self.target_ratio + y)) / (t + 1)
            if random() > self.subsample and (t + 1) % self.rate != 0:
                continue
            wtx = 0
            w = {}
            for indx in x.keys():
                
                if fabs(self.z[indx]) <= self.l1:
                    w[indx] = 0
                else:
                    sign = 1. if self.z[indx] >= 0 else -1.
                    w[indx] = - (self.z[indx] - sign * self.l1) / \
                                (self.l2 + (self.beta + sqrt(self.n[indx])) / self.alpha)
                wtx += w[indx] * x[indx]

            p = 1. / (1. + exp(-max(min(wtx, 35.), -35.)))

            self.log_likelihood_ += log_loss(y, p)

            if (self.rate > 0) and (t + 1) % self.rate == 0:
                self.loss.append(self.log_likelihood_)

                print('Training Samples: {0:9} | '
                      'Loss: {1:11.2f} | '
                      'Time taken: {2:4} seconds'.format(t + 1,
                                                         self.log_likelihood_,
                                                         (datetime.now() - start_time).seconds))


            self._update(y, p, x, w)

        self.coef_.update(dict([[key, self.z[data_gen.names[key]]] for key in data_gen.names.keys()]))


    def fit(self, data_gen, path):
        if self.fit_flag:
            self._clear_params()

        total_time = datetime.now()

        for epoch in range(self.epochs):
            epoch_time = datetime.now()

            if self.rate > 0:
                print('TRAINING EPOCH: {0:2}'.format(epoch + 1))
                print('-' * 18)

            self._train(data_gen, path)

            if self.rate > 0:
                print('EPOCH {0:2} FINISHED IN {1} seconds'.format(epoch + 1,
                                                                   (datetime.now() - epoch_time).seconds))
                print()


        if self.rate > 0:
            print(' --- TRAINING FINISHED IN '
                  '{0} SECONDS WITH LOSS {1:.2f} ---'.format((datetime.now() - total_time).seconds,
                                                             self.log_likelihood_))
            print()

        self.fit_flag = True

    def partial_fit(self, data_gen, path):
        self.fit_flag = False

        self.fit(data_gen, path)

    def predict_proba(self, data_gen, path):
        result = []

        start_time = datetime.now()

        for t, x in data_gen.test(path):
            
            wtx = 0
            w = {}
            for indx in x.keys():
                if fabs(self.z[indx]) <= self.l1:
                    w[indx] = 0
                else:
                    sign = 1. if self.z[indx] >= 0 else -1.
                    w[indx] = - (self.z[indx] - sign * self.l1) / \
                                (self.l2 + (self.beta + sqrt(self.n[indx])) / self.alpha)

                wtx += w[indx] * x[indx]

            result.append(1. / (1. + exp(-max(min(wtx, 35.), -35.))))

            if (t + 1) % self.rate == 0:
                print('Test Samples: {0:8} | '
                      'Time taken: {1:3} seconds'.format(t + 1,
                                                         (datetime.now() - start_time).seconds))

        return result

    def predict(self, data_gen, path):
        probs = self.predict_proba(data_gen, path)
        return map(lambda x: 0 if x <= self.target_ratio else 1, probs)
