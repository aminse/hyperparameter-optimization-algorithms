import math
import random
import typing
import matplotlib.pyplot as plt

class City:
    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y
    
    def distance(self, city) -> float:
        x_distance = abs(self.x - city.x)
        y_distance = abs(self.y - city.y)
        distance = math.sqrt((x_distance ** 2) + (y_distance ** 2))
        return distance
    
    def __repr__(self) -> str:
        return "(" + str(self.x) + "," + str(self.y) + ")"
    
class Fitness:
    def __init__(self, route: typing.List[City]) -> None:
        self.route = route
        self.distance = 0.0
        self.fitness= 0.0
    
    def route_distance(self) -> float:
        if self.distance ==0:
            path_distance = 0
            for i in range(0, len(self.route)):
                from_city = self.route[i]
                to_city = None
                if i + 1 < len(self.route):
                    to_city = self.route[i + 1]
                else:
                    to_city = self.route[0]
                path_distance += from_city.distance(to_city)
            self.distance = path_distance
        return self.distance
    
    def route_fitness(self) -> float:
        if self.fitness == 0:
            self.fitness = 1 / float(self.route_distance())
        return self.fitness
    
class TSP_GA:
    def __init__(self, population: typing.List[City], population_size: int, elite_size: int, mutation_rate: float, generations: int) -> None:
        self.population = population
        self.population_size = population_size
        self.elite_size = elite_size
        self.mutation_rate = mutation_rate
        self.generations = generations
    
    def create_route(self) -> typing.List[City]:
        route = random.sample(self.population, len(self.population))
        return route

    def starting_population(self) -> typing.List[typing.List[City]]:
        population = []

        for i in range(0, self.population_size):
            population.append(self.create_route())

        return population


    def rank_routes(self, population: typing.List[typing.List[City]]) -> typing.List[typing.Tuple[int, float]]:
        fitness_results = {}
        for i in range(0,len(population)):
            fitness_results[i] = Fitness(population[i]).route_fitness()
        return sorted(fitness_results.items(), key = lambda x: x[1], reverse = True)

    def selection(self, ranked_population: typing.List[typing.Tuple[City, float]], elite_size: int) -> typing.List[City]:
        selection_results = []

        for i in range(0, self.elite_size):
            selection_results.append(ranked_population[i][0])

        fitness_sum = sum([ranked_population[i][1] for i in range(0, len(ranked_population))])
        fitness_cumulative_sum = [ranked_population[0][1]]

        for i in range(1, len(ranked_population)):
            fitness_cumulative_sum.append(fitness_cumulative_sum[i - 1] + ranked_population[i][1])

        for i in range(0, len(ranked_population) - self.elite_size):
            pick = 100 * random.random()
            for i in range(0, len(ranked_population)):
                if pick <= (fitness_cumulative_sum[i] / fitness_sum) * 100:
                    selection_results.append(ranked_population[i][0])
                    break

        return selection_results

    def mating_pool(self, population: typing.List[City], selection_results: typing.List[City]) -> typing.List[City]:
        matingpool = []
        for i in range(0, len(selection_results)):
            index = selection_results[i]
            matingpool.append(population[index])
        return matingpool

    def breed(self, parent1: typing.List[City], parent2: typing.List[City]) -> typing.List[City]:
        child = []
        childP1 = []
        childP2 = []
        gene_a = int(random.random() * len(parent1))
        gene_b = int(random.random() * len(parent1))

        start_gene = min(gene_a, gene_b)
        end_gene = max(gene_a, gene_b)

        for i in range(start_gene, end_gene):
            childP1.append(parent1[i])

        childP2 = [item for item in parent2 if item not in childP1]

        child = childP1 + childP2
        return child

    def breed_population(self, matingpool: typing.List[typing.List[City]]) -> typing.List[typing.List[City]]:
        children = []
        length = len(matingpool) - self.elite_size
        pool = random.sample(matingpool, len(matingpool))

        for i in range(0,self.elite_size):
            children.append(matingpool[i])

        for i in range(0, length):
            child = self.breed(pool[i], pool[len(matingpool)-i-1])
            children.append(child)
        return children

    def mutate(self, individual: typing.List[City]) -> typing.List[City]:
        for swapped in range(len(individual)):
            if(random.random() < self.mutation_rate):
                swap_with = int(random.random() * len(individual))

                city1 = individual[swapped]
                city2 = individual[swap_with]

                individual[swapped] = city2
                individual[swap_with] = city1
        return individual


    def mutate_population(self, children: typing.List[City]) -> typing.List[City]:
        mutated_population = []

        for ind in range(0, len(children)):
            mutated_individual = self.mutate(children[ind])
            mutated_population.append(mutated_individual)
        return mutated_population

    def next_generation(self, current_generation: typing.List[City]) -> typing.List[City]:
        ranked_population = self.rank_routes(current_generation)
        selection_results = self.selection(ranked_population, self.elite_size)
        matingpool = self.mating_pool(current_generation, selection_results)
        children = self.breed_population(matingpool)
        next_generation = self.mutate_population(children)
        return next_generation

    def run(self) -> typing.List[City]:
        pop = self.starting_population()
        print("Initial distance: " + str(1 / self.rank_routes(pop)[0][1]))

        for i in range(0, self.generations):
            pop = self.next_generation(pop)

        print("Final distance: " + str(1 / self.rank_routes(pop)[0][1]))
        best_route_index = self.rank_routes(pop)[0][0]
        best_route = pop[best_route_index]
        return best_route
    
    def plot(self) -> None:
        x_s = []
        y_s = []
        for i in self.population:
            x_s.append(i.x)
            y_s.append(i.y)
        plt.scatter(x=x_s, y=y_s)
        plt.plot(x_s, y_s)
        
        best_route = self.run()
        x_s = []
        y_s = []
        for i in best_route:
            x_s.append(i.x)
            y_s.append(i.y)
        plt.scatter(x=x_s, y=y_s)
        plt.plot(x_s, y_s)
    

if __name__ == "__main__":
    list_of_cities = []
    for i in range(0,25):
        list_of_cities.append(City(x=int(random.random() * 200), y=int(random.random() * 200)))
    my_algo = TSP_GA(population=list_of_cities, population_size=150, elite_size=10, mutation_rate=0.02, generations=400)
    #my_algo.run()
    my_algo.plot()


