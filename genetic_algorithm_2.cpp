#include <iostream>
#include <math.h>

#include "Utils.h"

using namespace std;

float Himmelblau(float x, float y)
{
    return (pow(x, 2) + y - 11) * (pow(x, 2) + y - 11) + (x + pow(y, 2) - 7) * (x + pow(y, 2) - 7);
}

double* init_individual(double x_lower, double x_upper, double y_lower, double y_upper) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> x(x_lower, x_upper);
    std::uniform_real_distribution<> y(y_lower, y_upper);
    double* individual = new double[3];
    individual[0] = x(gen);
    individual[1] = y(gen);
    individual[2] = Himmelblau(individual[0], individual[1]);
    return individual;
}

double** init_population(int pop_size, double x_lower, double x_upper, double y_lower, double y_upper)
{
    double** population = new double*[pop_size * 3];
    for(int i = 0; i < pop_size; i++)
    {
        population[i] = init_individual(x_lower, x_upper, y_lower, y_upper);
    }
    return population;
}

//TODO fix selection
double** selection(double** population, int pop_size){
        int i,j;
        double temp;
        for (i = 0; i < pop_size-1; i++){
            for (j = i+1; j < pop_size; j++){
                if (population[i][2] > population[j][2]){
                    temp = population[i][0];
                    population[i][0] = population[j][0];
                    population[j][0] = temp;
                    temp = population[i][1];
                    population[i][1] = population[j][1];
                    population[j][1] = temp;
                    temp = population[i][2];
                    population[i][2] = population[j][2];
                    population[j][2] = temp;
                }
            }
        }
        return population;
}


double** crossover( double** population, double Pc)
{   
    int len_population = sizeof(population) / sizeof(population[0]);
    double** new_population =  new double*[len_population];
    std::mt19937 generator(std::random_device{}());
    std::uniform_int_distribution<std::size_t> distribution(0, len_population-1);
    for (int i = 0; i < len_population-1; i+= 2)
    {

        double avg_pop_fitness = avg_pop_fitness(population);
        
        double best_pop_fitness = best_pop_fitness(population);

        double probability = random_double(0, 1)
        if (probability < Pc)
        {
            try
            {
                int num_of_mating = (population[i][2] / best_pop_fitness * 10) < avg_pop_fitness ? (population[i][2] / best_pop_fitness * 10) : 1;
                for (int j = 1; j < num_of_mating; j++)
                {
                    double x1, y1, f1 = population[i];
                    double x2, y2, f2 = population[distribution(generator)];
                    double child1_x = (x1 + x2)/2;
                    double child1_y = (y1 + y2)/2;
                    double child1_f = Himmelblau(child1_x, child1_y);
                    double child2_x = (x1 - x2)/2;
                    double child2_y = (y1 - y2)/2;
                    double child2_f = Himmelblau(child2_x, child2_y);
                    new_population[i] = new double[3];
                    new_population[i][0] = child1_x;
                    new_population[i][1] = child1_y;
                    new_population[i][2] = child1_f;
                    new_population[i+1] = new double[3];
                    new_population[i+1][0] = child2_x;
                    new_population[i+1][1] = child2_y;
                    new_population[i+1][2] = child2_f;
                }
            }
            catch (ZeroDivisionError& e)
            {
                cout << population[i] << endl;
                break;
            }
        }
        else
        {
            new_population[i] = population[i];
            new_population[i+1] = population[distribution(generator)];
        }
    }
    return new_population;
}

int main()
{
    // int pop_size = 4;
    // double** my_pop = init_population(4, 0, 10, 0, 10);
    // double** sorted = selection(my_pop, 4);
    // delete my_pop;
    // cout << sorted[0][2] << "  " << sorted[1][2] << "  " << sorted[2][2] << " " << sorted[3][2] << endl;
    double zen = random_double(0, 1);
    cout << zen << endl;
}
