#include <iostream>
#include <cmath>
#include <vector>
using namespace std;


class City
{
public:
    float x;
    float y;

    City(City& other);
    City(float x, float y);

    float distance(City city);
};

City::City(const City& other)
{
    this->x = other.x;
    this->y = other.y;
}

City::City(float x, float y)
{
    this->x = x;
    this->y = y;
}


float City::distance(const City& city)
{
    float x_distance = abs(this->x - city.x);
    float y_distance = abs(this->y - city.y);
    float distance = sqrt((x_distance * x_distance) + (y_distance * y_distance));
    return distance;
}


class Fitness{
    public:
        Fitness(vector<City> route);
        float route_distance();
        float route_fitness();

    private:
        vector<City> route;
        float distance;
        float fitness;
};

Fitness::Fitness(const vector<City>& route)
{
    this->route = route;
    this->distance = 0;
    this->fitness = 0;
}

float Fitness::route_distance()
{
    if (this->distance == 0)
    {
        float path_distance = 0;
        for (int i = 0; i < this->route.size(); i++)
        {
            City from_city = this->route[i];
            City to_city = City(0.0, 0.0);
            if (i + 1 < this->route.size())
            {
                to_city = this->route[i + 1];
            }
            else
            {
                to_city = this->route[0];
            }
            path_distance += from_city.distance(to_city);
        }
        this->distance = path_distance;
    }
    return this->distance;
}

float Fitness::route_fitness()
{
    if (this->fitness == 0)
    {
        this->fitness = 1 / float(this->route_distance());
    }
    return this->fitness;
}

class TSP_GA
{
    public:
        TSP_GA(vector<City> population, int population_size, int elite_size, float mutation_rate, int generations);
        vector<City> create_route();
        vector<vector<City>> starting_population();
        vector<tuple<City, float>> rank_routes(vector<vector<City>> population);
        vector<City> selection(vector<tuple<City, float>> ranked_population, int elite_size);
        vector<City> mating_pool(vector<City> population, vector<City> selection_results);
        vector<City> breed(City parent1, City parent2);
        vector<City> breed_population(vector<City> matingpool);
        vector<City> mutate(vector<City> individual);
        vector<City> mutate_population(vector<City> children);
        vector<City> next_generation(vector<City> current_generation);
        vector<City> run();

    private:
        vector<City> population;
        int population_size;
        int elite_size;
        float mutation_rate;
        int generations;
};

TSP_GA::TSP_GA(const vector<City>& population, int population_size, int elite_size, float mutation_rate, int generations)
{
    this->population = population;
    this->population_size = population_size;
    this->elite_size = elite_size;
    this->mutation_rate = mutation_rate;
    this->generations = generations;
}

vector<City> TSP_GA::create_route() const
{
    vector<City> route = this->population;
    return route;
}

vector<vector<City>> TSP_GA::starting_population() const
{
    vector<vector<City>> population;

    for (int i = 0; i < this->population_size; i++)
    {
        population.push_back(this->create_route());
    }

    return population;
}


vector<tuple<City, float>> TSP_GA::rank_routes(vector<vector<City>> population)
{
    vector<tuple<City, float>> fitness_results;
    for (int i = 0; i < population.size(); i++)
    {
        fitness_results.push_back(make_tuple(population[i], Fitness(population[i]).route_fitness()));
    }
    return fitness_results;
}

vector<City> TSP_GA::selection(vector<tuple<City, float>> ranked_population, int elite_size)
{
    vector<City> selection_results;

    for (int i = 0; i < this->elite_size; i++)
    {
        selection_results.push_back(get<0>(ranked_population[i]));
    }

    float fitness_sum = 0;
    for (int i = 0; i < ranked_population.size(); i++)
    {
        fitness_sum += get<1>(ranked_population[i]);
    }
    float fitness_cumulative_sum = get<1>(ranked_population[0]);

    for (int i = 1; i < ranked_population.size(); i++)
    {
        fitness_cumulative_sum += get<1>(ranked_population[i]);
    }

    for (int i = 0; i < ranked_population.size() - this->elite_size; i++)
    {
        float pick = 100 * rand();
        for (int i = 0; i < ranked_population.size(); i++)
        {
            if (pick <= (fitness_cumulative_sum / fitness_sum) * 100)
            {
                selection_results.push_back(get<0>(ranked_population[i]));
                break;
            }
        }
    }

    return selection_results;
}

vector<City> TSP_GA::mating_pool(vector<City> population, vector<City> selection_results)
{
    vector<City> matingpool;
    for (int i = 0; i < selection_results.size(); i++)
    {
        for (int j = 0; j < population.size(); j++)
        {
            if (selection_results[i] == population[j])
            {
                matingpool.push_back(population[j]);
            }
        }
    }
    return matingpool;
}

vector<City> TSP_GA::breed(City parent1, City parent2)
{
    vector<City> child;
    vector<City> childP1;
    vector<City> childP2;
    int gene_a = int(rand() * parent1.size());
    int gene_b = int(rand() * parent1.size());

    int start_gene = min(gene_a, gene_b);
    int end_gene = max(gene_a, gene_b);

    for (int i = start_gene; i < end_gene; i++)
    {
        childP1.push_back(parent1[i]);
    }

    childP2 = [item for item in parent2 if item not in childP1];

    child = childP1 + childP2;
    return child;
}

vector<City> TSP_GA::breed(City parent1, City parent2)
{
    vector<City> child;
    vector<City> childP1;
    vector<City> childP2;
    int gene_a = int(rand() * parent1.size());
    int gene_b = int(rand() * parent1.size());

    int start_gene = min(gene_a, gene_b);
    int end_gene = max(gene_a, gene_b);

    for (int i = start_gene; i < end_gene; i++)
    {
        childP1.push_back(parent1[i]);
    }

    childP2 = [item for item in parent2 if item not in childP1];

    child = childP1 + childP2;
    return child;
}

vector<City> vector<City> TSP_GA::breed_population(const vector<City>& matingpool)
{
    vector<City> children;

    for (int i = 0; i < this->population_size; i++)
    {
        int r = int(rand() * matingpool.size());
        int s = int(rand() * matingpool.size());

        City parent1 = matingpool[r];
        City parent2 = matingpool[s];

        City child = this->breed(parent1, parent2);
        children.push_back(child);
    }
    return children;
}

vector<City> TSP_GA::mutate(vector<City> individual)
{
    int r = int(rand() * individual.size());
    int s = int(rand() * individual.size());

    City city1 = individual[r];
    City city2 = individual[s];

    individual[r] = city2;
    individual[s] = city1;

    return individual;
}

vector<City> TSP_GA::mutate_population(vector<City> children)
{
    vector<City> mutated_population = [];

    for (int i = 0; i < children.size(); i++)
    {
        vector<City> mutated_individual = this->mutate(children[i]);
        mutated_population.push_back(mutated_individual);
    }
    return mutated_population;
}

vector<City> TSP_GA::next_generation(const vector<City>& current_generation)
{
    vector<tuple<City, float>> ranked_population = this->rank_routes(current_generation);
    auto selection_results = this->selection(ranked_population, this->elite_size);
    auto matingpool = this->mating_pool(current_generation, selection_results);
    auto children = this->breed_population(matingpool);
    auto next_generation = this->mutate_population(children);
    return next_generation;
}

vector<City> TSP_GA::run() const
{
    vector<City> pop = this->starting_population();
    cout << "Initial distance: " << 1 / get<1>(this->rank_routes(pop)[0]) << endl;

    for (int i = 0; i < this->generations; i++)
    {
        pop = this->next_generation(pop);
    }

    cout << "Final distance: " << 1 / get<1>(this->rank_routes(pop)[0]) << endl;
    auto best_route_index = get<0>(this->rank_routes(pop)[0]);
    auto best_route = pop[best_route_index];
    return results;
}

int main()
{
    vector<City> population;
    population.push_back(City(0.0, 0.0));
    population.push_back(City(0.0, 1.0));
    population.push_back(City(0.0, 2.0));
    population.push_back(City(0.0, 3.0));
    population.push_back(City(1.0, 0.0));
    population.push_back(City(1.0, 1.0));
    population.push_back(City(1.0, 2.0));
    population.push_back(City(1.0, 3.0));
    population.push_back(City(2.0, 0.0));
    population.push_back(City(2.0, 1.0));
    population.push_back(City(2.0, 2.0));
    population.push_back(City(2.0, 3.0));
    population.push_back(City(3.0, 0.0));
    population.push_back(City(3.0, 1.0));
    population.push_back(City(3.0, 2.0));
    population.push_back(City(3.0, 3.0));

    TSP_GA tsp = TSP_GA(population, 4, 2, 0.01, 100);
    vector<City> results = tsp.run();
    return 0;
}


